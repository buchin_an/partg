package com.company;

import com.company.model.*;

public class FinanceSystem {
    Organization[] organizations;

    public FinanceSystem(Organization[] organizations) {
        this.organizations = organizations;
    }

    void convert(int amount, int choise, boolean isUA) {
        Organization bestOrganization = null;
        double bestValue = 0;
        for (Organization organization : organizations) {
            if (organization instanceof Exchangeble) {
                if (organization instanceof Bank) {
                    if (bestValue < ((Bank) organization).convert(amount, choise, isUA)) {
                        bestOrganization = organization;
                        bestValue = ((Bank) organization).convert(amount, choise, isUA);
                    }
                }
                if (organization instanceof CurrencyExchanger) {
                    if (bestValue < ((CurrencyExchanger) organization).convert(amount, choise, isUA)) {
                        bestOrganization = organization;
                        bestValue = ((CurrencyExchanger) organization).convert(amount, choise, isUA);
                    }
                }
            }

        }
        System.out.println("Best amount: " + bestValue);
        bestOrganization.print();
    }

    void credit(int amount, int number) {
        Organization bestOrganization = null;
        double bestValue = Double.MAX_VALUE;
        for (Organization organization : organizations) {
            if (organization instanceof Creditable) {
                if (organization instanceof CreditCafe) {
                    double payment = ((CreditCafe) organization).credit(amount, number);
                    if (bestValue > payment) {
                        bestValue = payment;
                        bestOrganization = organization;
                    }
                }
                if (organization instanceof PawnShop) {
                    double payment = ((PawnShop) organization).credit(amount, number);
                    if (bestValue > payment) {
                        bestValue = payment;
                        bestOrganization = organization;
                    }
                }
                if (organization instanceof CreditUnion) {
                    double payment = ((CreditUnion) organization).credit(amount, number);
                    if (bestValue > payment) {
                        bestValue = payment;
                        bestOrganization = organization;
                    }
                }
                if (organization instanceof Bank) {
                    double payment = ((Bank) organization).credit(amount, number);
                    if (bestValue > payment) {
                        bestValue = payment;
                        bestOrganization = organization;
                    }
                }
            }
        }
        System.out.println("best rate: " + bestValue);
        bestOrganization.print();
    }

    void deposite(int amount, int number) {
        Organization bestOrganization = null;
        double bestValue = 0;
        for (Organization organization : organizations) {
            if (organization instanceof Depositable) {
                if (organization instanceof OpenEndFund) {
                    if (bestValue < ((OpenEndFund) organization).deposite(amount, number)) {
                        bestValue = ((OpenEndFund) organization).deposite(amount, number);
                        bestOrganization = organization;
                    }
                }
                if (organization instanceof Bank) {
                    if (bestValue < ((Bank) organization).deposite(amount, number)) {
                        bestValue = ((Bank) organization).deposite(amount, number);
                        bestOrganization = organization;
                    }
                }
            }
        }
        System.out.println("best month rate " + bestValue);
        bestOrganization.print();
    }

    void send(int amount) {
        Organization bestOrganization = null;
        double bestValue = 0;
        for (Organization organization : organizations) {
            if (organization instanceof Sender) {
                if (organization instanceof PostOffice) {
                    if (bestValue < ((PostOffice) organization).send(amount)) {
                        bestValue = ((PostOffice) organization).send(amount);
                        bestOrganization = organization;
                    }
                }
                if (organization instanceof Bank) {
                    if (bestValue < ((Bank) organization).send(amount)) {
                        bestValue = ((Bank) organization).send(amount);
                        bestOrganization = organization;
                    }
                }
            }
        }
        System.out.println("delivered " + bestValue);
        bestOrganization.print();
    }
}
