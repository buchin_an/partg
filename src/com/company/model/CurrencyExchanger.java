package com.company.model;

public class CurrencyExchanger extends Organization implements Exchangeble {

    private float rateEUR;
    private float rateUSD;
    private float rateRU;

    public CurrencyExchanger(String name, String address, float rateEUR, float rateUSD, float rateRU) {
        super(name, address);
        this.rateEUR = rateEUR;
        this.rateUSD = rateUSD;
        this.rateRU = rateRU;
    }

    @Override
    public double convert(int amount, int choice, boolean isUA) {
        return new Exchanger(rateEUR, rateUSD, rateRU).convert(amount, choice, isUA);
    }

    @Override
    public void print() {
        super.print();
        System.out.println("Euro rate: " + rateEUR);
        System.out.println("USD rate: " + rateUSD);
        System.out.println("Ru rate: " + rateRU);
    }
}
