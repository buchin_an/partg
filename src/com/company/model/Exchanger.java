package com.company.model;

import static com.company.Main.EUR;
import static com.company.Main.RU;
import static com.company.Main.USD;

public class Exchanger {

    private float rateEUR;
    private float rateUSD;
    private float rateRU;

    public Exchanger(float rateEUR, float rateUSD, float rateRU) {
        this.rateEUR = rateEUR;
        this.rateUSD = rateUSD;
        this.rateRU = rateRU;
    }

    public double convert(int amount, int choice, boolean isUA) {

        if (!isUA) {
            switch (choice) {
                case EUR:
                    return amount * rateEUR;
                case USD:
                    return amount * rateUSD;
                case RU:
                    return amount * rateRU;
            }
        }
        if (isUA) {
            switch (choice) {
                case EUR:
                    return amount / rateEUR;
                case USD:
                    return amount / rateUSD;
                case RU:
                    return amount / rateRU;
            }
        }
        return 0;

    }
}
