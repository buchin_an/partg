package com.company.model;

public class CreditUnion extends Organization implements Creditable {
    int maxValue = 100_000;
    int percent = 20;

    public CreditUnion(String name, String address) {
        super(name, address);
    }

    @Override
    public double credit(int amount, int number) {
        if (amount < maxValue) {
            return percent / 12 * number;
        }
        return Double.MAX_VALUE;
    }

    @Override
    public void print() {
        super.print();
        System.out.println("max count " + maxValue);
        System.out.println("annual interest rate " + percent);
    }
}
