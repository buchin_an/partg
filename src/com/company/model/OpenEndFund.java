package com.company.model;

public class OpenEndFund extends Organization implements Depositable {
    private int year;

    int minNumber = 11;
    int percent = 20;

    public OpenEndFund(String name, String address, int year) {
        super(name, address);
        this.year = year;
    }

    @Override
    public double deposite(int amount, int number) {
        if (number > minNumber) {
            return percent / 12;
        }
        return 0;
    }

    @Override
    public void print() {
        super.print();
        System.out.println("year: " + year);
        System.out.println("deposit rate: " + percent);
    }
}
