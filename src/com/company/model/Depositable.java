package com.company.model;

public interface Depositable {
    public double deposite(int amount, int number);
}
