package com.company.model;

public class PostOffice extends Organization implements Sender {
    public PostOffice(String name, String address) {
        super(name, address);
    }

    @Override
    public double send(int amount) {
        return amount * .98;
    }

    @Override
    public void print() {
        super.print();
        System.out.println("Commission 2%");
    }
}
