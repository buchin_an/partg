package com.company.model;

public class Organization {
    private String name;
    private String address;

    public Organization(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public void print() {
        System.out.println("name: " + name);
        System.out.println("address " + address);

    }
}
