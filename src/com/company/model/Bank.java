package com.company.model;

public class Bank extends Organization implements Exchangeble, Creditable, Depositable, Sender {

    private float rateEUR;
    private float rateUSD;
    private float rateRU;

    private int maxValueExchange = 12000;
    private int commissionExchange = 15;

    int maxValueCredit = 200_000;
    int percent = 25;
    int depositeNumber = 12;

    public Bank(String name, String address, float rateEUR, float rateUSD, float rateRU) {
        super(name, address);
        this.rateEUR = rateEUR;
        this.rateUSD = rateUSD;
        this.rateRU = rateRU;
    }


    @Override
    public double convert(int amount, int choice, boolean isUA) {
        if (isUA) {
            if (amount < maxValueExchange) {
                return new Exchanger(rateEUR, rateUSD, rateRU).convert(amount - commissionExchange, choice, isUA);
            } else return 0;
        }
        return new Exchanger(rateEUR, rateUSD, rateRU).convert(amount, choice, isUA) - commissionExchange;
    }

    @Override
    public void print() {
        super.print();
    }

    @Override
    public double credit(int amount, int number) {
        if (amount < maxValueCredit) {
            return percent / 12 * number;
        }
        return Double.MAX_VALUE;
    }

    @Override
    public double deposite(int amount, int number) {
        if (number < depositeNumber) {
            return percent / 12;
        }
        return 0;
    }

    @Override
    public double send(int amount) {
        return amount * .99 - 5;
    }
}
