package com.company;

import com.company.model.*;

public class Generator {
    public Generator() {
    }

    public static Organization[] generate() {
        Organization organizations[] = new Organization[11];
        organizations[0] = new Bank("Private bank", "Somewhere 1", 25, 24, 0.4f);
        organizations[1] = new Bank("Public bank", "Somewhere 2", 26, 25, 0.6f);
        organizations[2] = new Bank("Protected bank", "Somewhere 3", 27, 28, 0.7f);
        organizations[3] = new CurrencyExchanger("alpha", "Somewhere 4", 20.5f, 28.6f, 0.7f);
        organizations[4] = new CurrencyExchanger("beta", "Somewhere 5", 21.1f, 28.3f, 0.7f);
        organizations[5] = new CurrencyExchanger("gamma", "Somewhere 6", 27f, 28.1f, 0.7f);
        organizations[6] = new PawnShop("PawnShop", "Somewhere 7");
        organizations[7] = new CreditCafe("CreditCafe", "Somewhere 8");
        organizations[8] = new CreditUnion("CreditUnion", "Somewhere 9");
        organizations[9] = new OpenEndFund("OpenEndFund", "Somewhere 10",1998);
        organizations[10] = new PostOffice("PostOffice", "Somewhere 11");

        return organizations;
    }
}
